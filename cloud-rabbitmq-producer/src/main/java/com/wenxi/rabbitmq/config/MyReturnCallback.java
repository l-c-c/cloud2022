package com.wenxi.rabbitmq.config;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

/**
 * @author swx
 * @date 2024-02-23 13:52
 * 在确认模式下，如果消息未能成功投递到任何队列，交换机会将消息退回给生产者，这时就会执行生产者端设置的returnedMessage回调函数来处理这些未送达的消息。
 */
public class MyReturnCallback implements RabbitTemplate.ReturnCallback {
    @Override
    public void returnedMessage(Message message, int replyCode, String replyText, String exchange, String routingKey) {
        System.err.println("A message was returned from the broker:");
        System.err.println("\tReply code: " + replyCode);
        System.err.println("\tReply text: " + replyText);
        System.err.println("\tExchange: " + exchange);
        System.err.println("\tRouting key: " + routingKey);
    }
}
