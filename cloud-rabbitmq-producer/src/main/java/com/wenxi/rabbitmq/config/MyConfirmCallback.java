package com.wenxi.rabbitmq.config;

import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import java.util.UUID;

/**
 * @author swx
 * @date 2024-02-23 13:56
 */
public class MyConfirmCallback implements RabbitTemplate.ConfirmCallback {
    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        if (correlationData == null) {
            // 如果没有提供CorrelationData，在发送时可以使用DefaultCorrelationData生成一个唯一ID
            System.out.println("Received confirmation for an anonymous message: " + ack);
        } else {
            String messageId = correlationData.getId();
            if (ack) {
                System.out.println("消息  id为 '" + messageId + "' 发送成功到broker.");
            } else {
                System.err.println("消息  id为 '" + messageId + "' 发送失败到broker: " + cause);
                // 在这里可以执行重试逻辑或记录错误日志等操作
            }
        }
    }


    // 为了在发送时关联每个消息，可以在调用convertAndSend时传递自定义的CorrelationData
    public CorrelationData generateCorrelationData() {
        return new CorrelationData(UUID.randomUUID().toString());
    }
}
