//package com.wenxi.rabbitmq.server;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.ApplicationArguments;
//import org.springframework.boot.ApplicationRunner;
//import org.springframework.stereotype.Component;
//
///**
// * @author swx
// * @date 2024-02-23 10:19
// */
//@Component
//public class PostConstructAfterRunner implements ApplicationRunner {
//
//    @Autowired
//    private ProducerServer producerServer;
//
//    @Override
//    public void run(ApplicationArguments args) throws Exception {
//        producerServer.serverSendMessage("测试消息");
//    }
//}
