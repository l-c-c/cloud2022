package com.wenxi.rabbitmq.server;

import com.wenxi.rabbitmq.config.MyConfirmCallback;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author swx
 * @date 2024-02-23 09:39
 */
@Service
public class ProducerServer {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    private static final String EXCHANGE = "myExchange";
    private static final String ROUTING_KEY = "myRoutingKey";


    public void serverSendMessage(String message,int ttlSeconds){
        CorrelationData correlationData = new MyConfirmCallback().generateCorrelationData();
        MessagePostProcessor messagePostProcessor = tempMsg -> {
            tempMsg.getMessageProperties().setExpiration(String.valueOf(ttlSeconds * 1000L));
            return tempMsg;
        };
        try {
            rabbitTemplate.convertAndSend(EXCHANGE,ROUTING_KEY,message,messagePostProcessor,correlationData);
        } catch (AmqpException e) {
            e.printStackTrace();
        }
    }


}
