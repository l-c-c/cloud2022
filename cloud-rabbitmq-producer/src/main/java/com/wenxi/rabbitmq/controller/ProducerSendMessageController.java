package com.wenxi.rabbitmq.controller;

import com.alibaba.fastjson.JSONObject;
import com.wenxi.rabbitmq.server.ProducerServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author swx
 * @date 2024-02-23 10:30
 */
@RestController
public class ProducerSendMessageController {

    @Autowired
    private ProducerServer producerServer;


    @PostMapping("/sendMess")
    public String sendMessageTest(@RequestBody JSONObject jsonObject) {
        ExecutorService executor = Executors.newFixedThreadPool(5);
        executor.execute(() -> {
            String message = Thread.currentThread().getId() + "------" + Thread.currentThread().getName();
            producerServer.serverSendMessage(message,10);
        });
        return "OK";
    }
}
