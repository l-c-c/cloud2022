//package com.wenxi.rabbitmq.controller;
//
//import org.springframework.amqp.core.Message;
//import org.springframework.amqp.rabbit.connection.CorrelationData;
//import org.springframework.amqp.rabbit.core.RabbitTemplate;
//import org.springframework.stereotype.Component;
//
///**
// * @author swx
// * @date 2024-02-22 18:05
// * @desction 发送消息确认：用来确认生产者 producer 将消息发送到 broker ，broker 上的交换机 exchange 再投递给队列 queue的过程中，消息是否成功投递。
// *
// * 消息从 producer 到 rabbitmq broker有一个 confirmCallback 确认模式。
// *
// * 消息从 exchange 到 queue 投递失败有一个 returnCallback 退回模式。
// *
// * 我们可以利用这两个Callback来确保消的100%送达。
// */
//@Component
//public class ProducerProcess implements RabbitTemplate.ConfirmCallback, RabbitTemplate.ReturnCallback{
//    @Override
//    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
//
//        /**
//         * correlationData：对象内部只有一个 id 属性，用来表示当前消息的唯一性。
//         * ack：消息投递到broker 的状态，true表示成功。
//         * cause：表示投递失败的原因。
//         */
//
//        if (!ack) {
//            System.out.println("消息发送异常!");
//        } else {
//            System.out.println("发送者已经收到确认，correlationData="+correlationData.getId());
//            System.out.println("发送者已经收到确认，correlationData="+correlationData.getReturnedMessage());
//            System.out.println("发送者已经收到确认，ack="+ack);
//            System.out.println("发送者已经收到确认，ack="+cause);
//        }
//    }
//
//    @Override
//    public void returnedMessage(Message message, int i, String s, String s1, String s2) {
//        System.out.println("退回的消息是："+new String(message.getBody()));
//    }
//}
