package com.wenxi.springcloud.myhandler;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.ultrapower.springcloud.domain.CommonResult;
import com.ultrapower.springcloud.domain.Payment;

/**
 * @auther shiwenxi
 * @date 2022-10-20
 */
public class CustomerHandler {

    public static CommonResult<?> handlerException(BlockException exception){
        return new CommonResult<>(444,"按客户要求的自定义处理类OK---GLOBAL handlerException---1");
    }

    public static CommonResult<?> handlerException2(BlockException exception){
        return new CommonResult<>(444,"按客户要求的自定义处理类OK---GLOBAL handlerException---2");
    }
}
