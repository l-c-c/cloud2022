package com.wenxi.springcloud.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * @auther shiwenxi
 * @date 2022-09-16:09
 */
@RestController
@Slf4j
public class FlowLimitController {


    @RequestMapping(value = "/testA",method = RequestMethod.GET)
    public String testA(){
        return "-------testA测试A";
    }


    @RequestMapping(value = "/testB",method = RequestMethod.GET)
    public String testB(){
        return "------testB测试B";
    }

    @RequestMapping("/testE")
    public String testE(){
        log.info("testE:异常数");
        int age = 10/0;
        return "----测试异常数";
    }

    @RequestMapping("/testF")
    public String testF(@RequestParam("id") String id){
        log.info("testF:热点参数是"+id);
        return "---测试热点参数"+id;
    }

    @GetMapping("/testHotKey")
    @SentinelResource(value = "testHotKey",blockHandler = "deal_testHotKey")
    public String testHotKeys(@RequestParam(value = "p1",required = false) String p1,
                              @RequestParam(value = "p2",required = false) String p2){
        return "------测试热点数据异常-------";
    }

    public String deal_testHotKey(String p1, String p2,BlockException exception){
        return "-----deal_testHotKey,😭";
    }
}
