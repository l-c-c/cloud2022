package com.wenxi.springcloud.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.ultrapower.springcloud.domain.CommonResult;
import com.ultrapower.springcloud.domain.Payment;
import com.wenxi.springcloud.myhandler.CustomerHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @auther shiwenxi
 * @date 2022-10-20
 */
@RestController
public class RateLimitController {

    @GetMapping("/byResource")
    @SentinelResource(value = "byResource", blockHandler = "handlerException")
    public CommonResult<?> byResource() {
        return new CommonResult<>(200, "按资源名称限流测试ok", new Payment(2002, "serial001"));
    }

    public CommonResult<?> handlerException(BlockException exception) {
        return new CommonResult<>(444, exception.getClass().getCanonicalName() + "\t服务不可用");
    }


    /**
     * 根据url进行流控
     *
     * @return json
     */
    @GetMapping("/rateLimit/byUrl")
    @SentinelResource(value = "byUrl")
    public CommonResult<?> byUrl() {
        return new CommonResult<>(200, "按URL限流测试OK", new Payment(2020, "serial1002"));
    }

    /**
     * 自定义的异常处理类测试控制器
     * @return JSON
     */
    @GetMapping("/rateLimit/customerBlockHandler")
    @SentinelResource(value = "customerBlockHandler",blockHandlerClass = CustomerHandler.class,blockHandler = "handlerException2")
    public CommonResult<?> customerBlockHandler() {
        return new CommonResult<>(200, "客户自定义处理类测试", new Payment(2003, "serial2003"));
    }

}
