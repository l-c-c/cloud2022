package com.wenxi.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author shiwenxi
 * @date 2022-05-24 22:17
 */
@SpringBootApplication
@EnableFeignClients  //激活openfeign功能,作为微服务客户端的注解,默认集成Ribbon + Eureka,用于实现负载均衡和服务发现
public class OrderFeignMain80 {
	public static void main(String[] args) {
		SpringApplication.run(OrderFeignMain80.class,args);
	}
}
