package com.wenxi.springcloud.service;

import com.ultrapower.springcloud.domain.CommonResult;
import com.ultrapower.springcloud.domain.Payment;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *openFeign使用步骤:
 *                  1.首先在业务层制定接口定义(同时需要加上指定的访问方式注解,以及访问地址)
 *                  2.直接定义指定的controller调用业务层接口返回数据,就完成openFeign使用
 * @author shiwenxi
 * @date 2022-05-24 22:24
 */
@Component
@FeignClient(value = "PAYMENT-SERVICE")  //调用微服务注册名
public interface PaymentFeignService {

	/**
	 * 根据id查询资源
	 *
	 * @param id 传入id
	 * @return 封装好的Payment
	 */
	@GetMapping(value = "/payment/get/{id}")
	CommonResult<Payment> getPaymentById(@PathVariable("id") String id);


	/**
	 * 获取全部资源
	 *
	 * @return list
	 */
	@GetMapping(value = "/payment/getAll")
	CommonResult<List<Payment>> getAll();

	/**
	 * 添加資源
	 * @param payment 资源对象
	 * @return  成功判断依据
	 */
	@RequestMapping(value = "/payment/create",method = RequestMethod.POST)
	CommonResult<Object> create(@RequestBody Payment payment);

	@GetMapping(value = "/payment/feign/timeout")
	String paymentFeignTimeout();
}
