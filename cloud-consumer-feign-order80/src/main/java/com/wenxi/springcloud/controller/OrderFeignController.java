package com.wenxi.springcloud.controller;

import com.ultrapower.springcloud.domain.CommonResult;
import com.ultrapower.springcloud.domain.Payment;
import com.wenxi.springcloud.service.PaymentFeignService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * openFeign面向接口编程
 * @author shiwenxi
 * @date 2022-05-24 23:13
 */
@RestController
@Slf4j
public class OrderFeignController {

	@Resource
	private PaymentFeignService paymentFeignService;

	@GetMapping(value = "/consumer/payment/get/{id}")
	public CommonResult<Payment> getPaymentById(@PathVariable("id") String id){
		return paymentFeignService.getPaymentById(id);
	}

	@GetMapping(value = "/consumer/payment/getAll")
	public CommonResult<List<Payment>> getAll(){
		return paymentFeignService.getAll();
	}


	@RequestMapping(value = "/consumer/payment/create",method = RequestMethod.POST)
	public CommonResult<Object> create(@RequestBody Payment payment){
		return paymentFeignService.create(payment);
	}

	@GetMapping(value = "/consumer/payment/feign/timeout")
	public String paymentFeignTimeout(){
		return paymentFeignService.paymentFeignTimeout();
	}
}
