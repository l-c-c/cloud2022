package com.wenxi.cloud.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author shiwenxi
 * @date 2022-07-18 22:27
 */
@Controller
@RefreshScope
public class ConfigClientController {

	@Value("${config.infos}")
	private String configInfos;

	@ResponseBody
	@RequestMapping ("/config/info")
	public String getConfigInfo(){
		return configInfos;
	}
}
