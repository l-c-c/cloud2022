package com.wenxi.cloud.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * @author shiwenxi
 * @date 2022-05-18 20:31
 */
@RestController
@Slf4j
public class PaymentController {

	@Value("${server.port}")
	private String serverPort;


	@RequestMapping(value = "/payment/zk")
	public String paymentZk(){
		return "springcloud with zookeeper:" + serverPort + "\t" + UUID.randomUUID().toString();
	}

}
