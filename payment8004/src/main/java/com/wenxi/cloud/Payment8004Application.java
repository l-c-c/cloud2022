package com.wenxi.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author shiwenxi
 * @data 2022 5-18
 */
@SpringBootApplication
@EnableDiscoveryClient   //使用consul或者zookeeper作为注册中心的时候使用
public class Payment8004Application {

	public static void main(String[] args) {
		SpringApplication.run(Payment8004Application.class, args);
	}

}
