# springcloud技术栈学习

#### 介绍
关联个人学习SpringCloud技术栈的代码demo

#### 软件架构
软件架构主要是SpringCloud技术栈的全栈技术的demo演示


#### 安装教程

1.  拉取对应的仓库地址
2.  对对应的数据库进行本地化
3.  启动服务即可


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 个人仓库用于自己使用
