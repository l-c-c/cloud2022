package com.wenxi.springcloud.controller;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.wenxi.springcloud.service.PaymentFeignHystrixService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author shiwenxi
 * @date 2022-06-10 16:17
 */
@RestController
@Slf4j
@DefaultProperties(defaultFallback = "paymentGlobalFallbackMethod")   //全局通用的fallback
public class ConsumerHystrixController {

	@Resource
	private PaymentFeignHystrixService paymentFeignHystrixService;

	@GetMapping(value = "/consumer/payment/hystrix/ok/{id}")
	public String paymentInfoOk(@PathVariable("id") Integer id){
		return paymentFeignHystrixService.paymentInfo_Ok(id);
	}


	@GetMapping(value = "/consumer/payment/hystrix/timeout/{id}")
//	@HystrixCommand(fallbackMethod = "paymentTimeOutFallbackMethod",commandProperties = {
//			@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds",value = "1500")
//	})
	@HystrixCommand
	public String paymentInfoTimeOut(@PathVariable("id") Integer id){
		int age = 10/0;
		//进来就报错
		return paymentFeignHystrixService.paymentInfo_TimeOut(id);
	}

	public String paymentTimeOutFallbackMethod(@PathVariable("id") Integer id){
		return "我是消费者80,对方支付系统在繁忙请10秒钟以后重试或检查自身错误";
	}

	/**
	 * 全局fallback方法
	 * @return str
	 */
	public String paymentGlobalFallbackMethod(){
		return "Global异常处理信息,请稍后重试!!!";
	}
}
