package com.wenxi.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author shiwenxi
 * @date 2022-06-10 16:08
 */
@SpringBootApplication
@EnableFeignClients
@EnableHystrix
public class ConsumerHystrix80 {
	public static void main(String[] args) {
		SpringApplication.run(ConsumerHystrix80.class,args);
	}
}
