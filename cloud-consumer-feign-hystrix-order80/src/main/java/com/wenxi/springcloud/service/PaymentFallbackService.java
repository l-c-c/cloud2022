package com.wenxi.springcloud.service;

import org.springframework.stereotype.Component;

/**
 * @author shiwenxi
 * @date 2022-06-14 14:13
 */
@Component
public class PaymentFallbackService implements PaymentFeignHystrixService{
	/**
	 * Feign使用步骤,接口加上注解,直接调用
	 *
	 * @param id
	 */
	@Override
	public String paymentInfo_Ok(Integer id) {
		return "-------------------PaymentFallbackService fallback-paymentInfo_Ok";
	}

	@Override
	public String paymentInfo_TimeOut(Integer id) {
		return "-------------------PaymentFallbackService fallback-paymentInfo_TimeOut";
	}
}
