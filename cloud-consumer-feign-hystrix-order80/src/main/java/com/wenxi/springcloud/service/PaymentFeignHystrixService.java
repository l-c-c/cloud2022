package com.wenxi.springcloud.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author shiwenxi
 * @date 2022-06-10 16:07
 */
@Component
@FeignClient(value = "CLOUD-PROVIDER-HYSTRIX-PAYMENT",fallback = PaymentFallbackService.class) //指定Feign接口中所有方法的fallback
public interface PaymentFeignHystrixService {
	/**
	   Feign使用步骤,接口加上注解,直接调用
	 */
	@GetMapping(value = "/payment/hystrix/ok/{id}")
	String paymentInfo_Ok(@PathVariable("id") Integer id);

	@GetMapping(value = "/payment/hystrix/timeout/{id}")
	String paymentInfo_TimeOut(@PathVariable("id") Integer id);
}
