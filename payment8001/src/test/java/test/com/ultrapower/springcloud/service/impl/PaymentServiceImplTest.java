package test.com.ultrapower.springcloud.service.impl; 

import com.ultrapower.springcloud.dao.PaymentMapper;
import com.ultrapower.springcloud.domain.Payment;
import com.ultrapower.springcloud.service.PaymentService;
import com.ultrapower.springcloud.service.impl.PaymentServiceImpl;
import org.apache.ibatis.annotations.Param;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import test.com.ultrapower.springcloud.BaseTest;

import javax.annotation.Resource;

/** 
* PaymentServiceImpl Tester. 
* 
* @author <S-W-X> 
* @since <pre>12/17/2021</pre> 
* @version 1.0 
*/
public class PaymentServiceImplTest extends BaseTest {


@Before
public void before() throws Exception { 
} 

@After
public void after() throws Exception { 
} 

/** 
* 
* Method: deleteByPrimaryKey(Integer id) 
* 
*/ 
@Test
public void testDeleteByPrimaryKey() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: insert(Payment record) 
* 
*/ 
@Test
public void testInsert() throws Exception { 
//TODO: Test goes here...
	/*Payment payment = new Payment();
	payment.setSerial("asdaedd3e32eedwedasd");

	System.out.println(paymentMapper.insert(payment));*/
}

/** 
* 
* Method: insertSelective(Payment record) 
* 
*/ 
@Test
public void testInsertSelective() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: selectByPrimaryKey(Integer id) 
* 
*/ 
@Test
public void testSelectByPrimaryKey() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: updateByPrimaryKeySelective(Payment record) 
* 
*/ 
@Test
public void testUpdateByPrimaryKeySelective() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: updateByPrimaryKey(Payment record) 
* 
*/ 
@Test
public void testUpdateByPrimaryKey() throws Exception { 
//TODO: Test goes here... 
} 


} 
