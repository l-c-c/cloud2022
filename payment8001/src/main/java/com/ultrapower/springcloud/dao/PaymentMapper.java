package com.ultrapower.springcloud.dao;
import org.apache.ibatis.annotations.Param;

import com.ultrapower.springcloud.domain.Payment;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author shiwenxi
 * @date 2021-12-24 18:33
 */
@Mapper
public interface PaymentMapper {
	int deleteByPrimaryKey(Integer id);

	int insert(Payment record);

	int insertSelective(Payment record);

	Payment selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(Payment record);

	int updateByPrimaryKey(Payment record);

	List<Payment> getAll();

}