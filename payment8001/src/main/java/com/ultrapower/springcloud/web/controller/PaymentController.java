package com.ultrapower.springcloud.web.controller;

import com.ultrapower.springcloud.domain.CommonResult;
import com.ultrapower.springcloud.domain.Payment;
import com.ultrapower.springcloud.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 对于前后端分离的项目,我们一般会使用@RestController
 * 主要是为处理只返回json数据不进行页面跳转,因为对于
 * 前后端分离的项目,我们后端只是返回数据,而不用去关心页面的跳转即使用该注解的时候会
 * 使得视图解析器无法解析对应的页面视图
 *
 * @author shiwenxi
 * @date 2021-12-17 14:33
 */
@RestController
@Slf4j
public class PaymentController {

	@Autowired
	private PaymentService paymentService;

	@Value("${server.port}")
	private String serverPort;

	@Resource
	private DiscoveryClient discoveryClient;
	/**
	 * 用于添加对应的账单参数
	 * //@requestParam注解的使用是将请求中的指定参数传递给对应的控制器的参数
	 *
	 * @return 封装对象
	 */
	@PostMapping(value = "/payment/create")
	public CommonResult<Object> create(@RequestBody Payment payment) {
//		@RequestBody 是获取对应的参数头的参数,为了使客户端可以通过url将对应的参数传递过去
//		所以可以使对应的客户端成功添加对应的参数
		int result = paymentService.insert(payment);
		log.info("插入结果为:" + result);
		if (result > 0) {
			return new CommonResult<>(201, "添加成功!", result);
		} else {
			return new CommonResult<>(444, "添加失败!", null);
		}
	}


	/**
	 * //@PathVariable可以映射对应的url中{}占位符的参数
	 *
	 * @param id 传入的id
	 * @return 查询信息
	 */

	@GetMapping(value = "/payment/get/{id}")
	public CommonResult<Object> getPaymentById(@PathVariable("id") String id) {
		int pid = Integer.parseInt(id);
		Payment payment = paymentService.selectByPrimaryKey(pid);
		log.info("查询结果为" + payment);
		if (payment != null) {
			return new CommonResult<>(200, "查询成功!,serverPost="+serverPort, payment);
		} else {
			return new CommonResult<>(204, "未查询到指定结果", null);
		}
	}


	/**
	 * 删除对应的资源
	 *
	 * @param id id
	 * @return 删除信息
	 */
	@DeleteMapping(value = "/payment/delete/{id}")
	public CommonResult<Object> delete(@PathVariable("id") String id) {
		Integer did = Integer.parseInt(id);
		int i = paymentService.deleteByPrimaryKey(did);
		log.info("删除结果为:" + i);
		if (i > 0) {
			return new CommonResult<>(200, "删除成功!", i);
		} else {
			return new CommonResult<>(400, "删除失败!", null);
		}
	}


	@PutMapping(value = "/payment/put/{id}")
	public CommonResult<Object> update(@PathVariable("id") String id, @RequestParam("Payment") Payment payment) {
		int i = paymentService.updateByPrimaryKey(payment);
		if (i > 0) {
			Payment payment1 = paymentService.selectByPrimaryKey(Integer.parseInt(id));
			return new CommonResult<>(200,"修改成功!",payment1);
		}else{
			Payment payment2 = paymentService.selectByPrimaryKey(Integer.parseInt(id));
			return new CommonResult<>(400,"修改失败!",payment2);
		}
	}

	@GetMapping(value = "/payment/getAll")
	public CommonResult<Object> getAll(){
		List<Payment> all = paymentService.getAll();
		if(!all.isEmpty()){
			return new CommonResult<>(200,"查询成功!",all);
		}else{
			return new CommonResult<>(400,"查询失败!",null);
		}
	}

	/**
	 * 返回
	 * @return
	 */
	@GetMapping(value = "/payment/discovery")
	public Object discovery(){
		List<String> services = discoveryClient.getServices();
		//返回在eureka注册的所有的设备
		for (String service : services) {
			log.warn("**********service:" + service);
		}
		List<ServiceInstance> instances = discoveryClient.getInstances("PAYMENT-SERVICE");
		for (ServiceInstance instance : instances) {
			log.warn("***********instance:"+instance.getServiceId() + "\t" + instance.getHost() + "\t" + instance.getPort() + "\t" + instance.getUri());
		}
		return this.discoveryClient;
	}

	@GetMapping(value = "/payment/lb")
	public String getPaymentLb(){
		return "<h1 style='color:red'>" + serverPort + "<h1/>";
	}

	@GetMapping(value = "/payment/feign/timeout")
	public String paymentFeignTimeout(){
		try {
			TimeUnit.SECONDS.sleep(3);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return serverPort;
	}
}
