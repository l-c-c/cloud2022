package com.ultrapower.springcloud.service.impl;

import com.ultrapower.springcloud.dao.PaymentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ultrapower.springcloud.domain.Payment;
import com.ultrapower.springcloud.service.PaymentService;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author shiwenxi
 * @date 2021-12-17 11:22
 */
@Service
public class PaymentServiceImpl implements PaymentService {
@Resource
private PaymentMapper paymentMapper;

	@Override
	public int deleteByPrimaryKey(Integer id) {
		return paymentMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(Payment record) {
		return paymentMapper.insert(record);
	}

	@Override
	public int insertSelective(Payment record) {
		return paymentMapper.insertSelective(record);
	}

	@Override
	public Payment selectByPrimaryKey(Integer id) {
		return paymentMapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(Payment record) {
		return paymentMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(Payment record) {
		return paymentMapper.updateByPrimaryKey(record);
	}

	@Override
	public List<Payment> getAll() {
		return paymentMapper.getAll();
	}


}



