package com.ultrapower.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 *
 * 启动主程序入口
 * @author shiwenxi
 * @date 2021-12-17 11:04
 */
@SpringBootApplication
@EnableEurekaClient
@EnableDiscoveryClient
public class PaymentMain {
//	配置入口程序
	public static void main(String[] args) {
		SpringApplication.run(PaymentMain.class,args);
	}
}
