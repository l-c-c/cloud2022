package com.wenxi.rabbitmq.client;

import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @author swx
 * @date 2024-02-23 10:26
 */
@Component
public class ConsumerServer{
    @RabbitListener(queuesToDeclare = @Queue(value = "myQueue")) // 替换为实际队列名
    public void consumeMessage(Message message, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long deliveryTag) {
        System.out.println("处理消息");
        System.out.println("收到消息: " + message);
        try {
            //消息处理成功
            channel.basicAck(deliveryTag,false);
        } catch (IOException e) {
            e.printStackTrace();
//            / 如果处理失败，可以选择重新入队或者丢弃（根据业务需求）
//            channel.basicNack(deliveryTag, false, true); // 拒绝并重新入队
//            // 或者
//            channel.basicReject(deliveryTag, false); // 直接丢弃
        }
        System.out.println("完成消息处理");
    }
}
