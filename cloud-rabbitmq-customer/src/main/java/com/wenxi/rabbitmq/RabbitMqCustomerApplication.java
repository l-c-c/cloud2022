package com.wenxi.rabbitmq;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author swx
 * @date 2024-02-21 15:19
 */
@SpringBootApplication
@EnableRabbit
public class RabbitMqCustomerApplication {
    public static void main(String[] args) {
        SpringApplication.run(RabbitMqCustomerApplication.class,args);
    }
}
