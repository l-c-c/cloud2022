package com.cloud.wenxi.config;

import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @auther shiwenxi
 * @date 2022-10-20
 */
@Configuration
public class ApplicationContextConfig {


//    //用于创建远程调用的负载均衡的对象
//    @Bean
//    @LoadBalanced
//    public RestTemplate getRestTemplate(){
//        return new RestTemplate();
//    }
//    @Bean
//    public ReactorLoadBalancer<ServiceInstance> reactorLoadBalancer(Environment environment, LoadBalancerClientFactory loadBalancerClientFactory){
//        String name = environment.getProperty(LoadBalancerClientFactory.PROPERTY_NAME);
//        return new RoundRobinLoadBalancer(loadBalancerClientFactory.getLazyProvider(name, ServiceInstanceListSupplier.class),name);
//    }

    @Bean
    Logger.Level feignLoggerLevel(){
        return Logger.Level.FULL;
    }

    /**
     * 指定负载均衡算法,可以自定义
     */
//    @Bean
//    public IRule myRule(){
//        //需要返回随机的Rule对象
//        return new RoundRobinRule();
//    }



}
