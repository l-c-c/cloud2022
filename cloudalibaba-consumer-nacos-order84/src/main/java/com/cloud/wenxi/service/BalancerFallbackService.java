package com.cloud.wenxi.service;

import com.ultrapower.springcloud.domain.CommonResult;
import com.ultrapower.springcloud.domain.Payment;
import org.springframework.stereotype.Component;

/**
 * @auther shiwenxi
 * @date 2022-10-23
 * 统一兜底的
 */
@Component
public class BalancerFallbackService implements BalancerService{
    @Override
    public CommonResult<?> paymentSQL(Long id) {
        return new CommonResult<>(444444,"服务降级返回,---BalancerFallbackService",new Payment(id.intValue(),"error"));
    }
}
