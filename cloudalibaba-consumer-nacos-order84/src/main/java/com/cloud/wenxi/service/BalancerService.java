package com.cloud.wenxi.service;

import com.ultrapower.springcloud.domain.CommonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @auther shiwenxi
 * @date 2022-10-21
 */
@Component
@FeignClient(value = "nacos-payment-provider",fallback = BalancerFallbackService.class)
public interface BalancerService {

    @GetMapping(value = "/paymentSQL/{id}")
    CommonResult<?> paymentSQL(@PathVariable("id") Long id);
}
