package com.wenxi.springcloud.service;

import cn.hutool.core.util.IdUtil;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.concurrent.TimeUnit;

/**
 * @author shiwenxi
 * @date 2022-06-09 15:14
 */
@Service
public class PaymentService{
	/**
	 * 正常访问的方法
	 * @param id
	 * @return str
	 */
	public String paymentInfo_Ok(Integer id){
		return "线程池:" + Thread.currentThread().getName() + "paymentInfo_Ok,id" + id + "\t" + "O(∩_∩)O哈哈~";
	}

	/**
	 * 服务降级使用   指定降级之后调用的方法,降级的条件是线程超时不超过三秒
	 * @param id
	 * @return
	 */
	@HystrixCommand(fallbackMethod = "paymentInfo_TimeOutHandler",commandProperties = {
			@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds",value = "5000")
			//如果该线程超时或等于指定的时间就会触发熔断降级
	})
	public String paymentInfo_TimeOut(Integer id){
//			int age = 10/0;
		int timeNumber = 3;
		//指定该接口的线程耗时5秒钟
		try {
			TimeUnit.SECONDS.sleep(timeNumber);
			//让该线程睡眠耗时指定的时间
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return "线程池:" + Thread.currentThread().getName() + "paymentInfo_TimeOut,id" + id + "\t" + "O(∩_∩)O哈哈~" + "耗时(秒)";
	}

	/**
	 * 服务降级后指定的方法
	 * @param id
	 * @return
	 */
	public String paymentInfo_TimeOutHandler(Integer id){
		return "线程池:" + Thread.currentThread().getName() + "paymentInfo_TimeOutHandler,id" + id + "\t" + "o(╥﹏╥)呜呜呜服务降级了";
	}

	//服务熔断

	/**
	 * 服务的熔断
	 * @return  str
	 */

	@HystrixCommand(fallbackMethod = "paymentCircuitBreaker_fallback",commandProperties = {
			@HystrixProperty(name = "circuitBreaker.enabled",value = "true"), //开启断路器
			@HystrixProperty(name = "circuitBreaker.requestVolumeThreshold",value = "10"), //请求次数
			@HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds",value = "10000"), //时间窗口期
			@HystrixProperty(name = "circuitBreaker.errorThresholdPercentage",value = "60"), //失败率达到多少的时候跳闸
	})
	public String paymentCircuitBreaker(@PathVariable("id") Integer id){
		if(id < 0){
			throw new RuntimeException("*****id  不能为负数");
		}
		String serialNumber = IdUtil.simpleUUID();
		return Thread.currentThread().getName() + "\n" + "调用成功!,流水号: " + serialNumber;
	}

	public String paymentCircuitBreaker_fallback(@PathVariable("id") Integer id){
		return "id  不能为负数,请稍后重试,  呜呜呜~~~~" + id;
	}
}
