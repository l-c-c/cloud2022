package com.wenxi.springcloud;

import com.netflix.hystrix.contrib.metrics.eventstream.HystrixMetricsStreamServlet;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;

/**
 * @author shiwenxi
 * @date 2022-06-09 15:12
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@EnableEurekaClient  //注册进eureka
@EnableCircuitBreaker //开启服务熔断降级
public class PaymentHystrix8001 {
	public static void main(String[] args) {
		SpringApplication.run(PaymentHystrix8001.class,args);
		System.out.println("**************************8001  启动成功!!!!!!**********************");
	}

	/**
	 * 配置对应的服务监控,便于监控服务器9001能够发现指定的服务端8001
	 * 跟服务容错无关,spring cloud升级之后的坑
	 * ServletRegistrationBean因为springboot的默认路径不是"/hystrix.stream"
	 * 只要在自己的项目中配置对应的上下文里面的servlet的配置就行
	 * @return bean
	 */
	@Bean
	public ServletRegistrationBean getServlet(){
		HystrixMetricsStreamServlet servlet = new HystrixMetricsStreamServlet();
		ServletRegistrationBean<HystrixMetricsStreamServlet> registrationBean = new ServletRegistrationBean<>(servlet);
		registrationBean.setLoadOnStartup(1);
		registrationBean.addUrlMappings("/hystrix.stream");
		registrationBean.setName("HystrixMetricsStreamServlet");
		return registrationBean;
	}
}
