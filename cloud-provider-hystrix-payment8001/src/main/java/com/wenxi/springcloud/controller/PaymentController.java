package com.wenxi.springcloud.controller;

import com.wenxi.springcloud.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author shiwenxi
 * @date 2022-06-09 15:23
 */
@RestController
@Slf4j
public class PaymentController {

	@Resource
	private PaymentService paymentService;


	/**
	 * 获取配置文件中的属性
	 */
	@Value("${server.port}")
	private String serverPort;


	@GetMapping(value = "/payment/hystrix/ok/{id}")
	public String paymentInfo_Ok(@PathVariable("id") Integer id){
		String result = paymentService.paymentInfo_Ok(id);
		log.warn("*****result:" + result);
		return result;
	}

	@GetMapping(value = "/payment/hystrix/timeout/{id}")
	public String paymentInfo_TimeOut(@PathVariable("id") Integer id){
		String s = paymentService.paymentInfo_TimeOut(id);
		log.warn("*******result:" + s);
		return s;
	}
	@GetMapping(value = "/payment/circuit/{id}")
	public String paymentCircuitBreaker(@PathVariable("id") Integer id){
		String result = paymentService.paymentCircuitBreaker(id);
		log.info("***********************************:result:{}",result);
		return result;
	}
}
