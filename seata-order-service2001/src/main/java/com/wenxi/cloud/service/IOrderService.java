package com.wenxi.cloud.service;

import com.wenxi.cloud.domain.Order;

/**
 * @auther shiwenxi
 * @date 2022-10-26
 */
public interface IOrderService {


    void create(Order order);

    void update(Long userId,Integer status);
}
