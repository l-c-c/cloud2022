package com.wenxi.cloud.service.impl;

import com.wenxi.cloud.dao.OrderDao;
import com.wenxi.cloud.domain.Order;
import com.wenxi.cloud.service.IAccountService;
import com.wenxi.cloud.service.IOrderService;
import com.wenxi.cloud.service.IStorageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @auther shiwenxi
 * @date 2022-10-26
 */
@Service
@Slf4j
public class OrderServiceImpl implements IOrderService {

    @Resource
    private OrderDao orderDao;

    @Resource
    private IStorageService storageService;

    @Resource
    private IAccountService accountService;

    @Override
    public void create(Order order) {
        log.info("------>开始新建订单");
        orderDao.create(order);
        log.info("------>订单微服务开始调用库存,做扣减");
    }

    @Override
    public void update(Long userId, Integer status) {

    }
}
