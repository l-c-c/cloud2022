package com.wenxi.cloud.dao;

import com.wenxi.cloud.domain.Order;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @auther shiwenxi
 * @date 2022-10-25
 */
@Mapper
public interface OrderDao {

    //新建订单
    void create(@Param("order") Order order);

    //修改订单状态
    void update(@Param("userId") Long userId,@Param("status") Integer status);
}
