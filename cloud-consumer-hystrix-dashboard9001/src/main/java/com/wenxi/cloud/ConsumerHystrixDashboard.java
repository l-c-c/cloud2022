package com.wenxi.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

/**
 * @author S-W-X
 * @create 2022-06-24 8:59
 */
@SpringBootApplication
@EnableHystrixDashboard  //开启Hystrix的图形化web界面
public class ConsumerHystrixDashboard {
	public static void main(String[] args) {
		SpringApplication.run(ConsumerHystrixDashboard.class,args);
		System.out.println("**************************9001 Hystrix监控启动成功!!!!!!**********************");
	}
}
