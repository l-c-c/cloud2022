package com.wenxi.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * @author shiwenxi
 * @date 2022-07-11 15:24
 */
@SpringBootApplication
@EnableConfigServer     //激活对应的配置中心
public class ConfigCenterMain3344 {
	public static void main(String[] args) {
		SpringApplication.run(ConfigCenterMain3344.class,args);
	}
}
