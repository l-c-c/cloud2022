package com.ultrapower.springcloud.service;

import com.ultrapower.springcloud.domain.Payment;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author shiwenxi
 * @date 2021-12-17 11:22
 */
public interface PaymentService {


	int deleteByPrimaryKey(Integer id);

	int insert(Payment record);

	int insertSelective(Payment record);

	Payment selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(Payment record);

	int updateByPrimaryKey(Payment record);

	List<Payment> getAll();
}



