package com.ultrapower.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author shiwenxi
 * @date 2022-01-18 23:50
 */
@SpringBootApplication
@EnableEurekaClient
public class Payment8002Application {
	public static void main(String[] args) {
		SpringApplication.run(Payment8002Application.class,args);
	}
}
