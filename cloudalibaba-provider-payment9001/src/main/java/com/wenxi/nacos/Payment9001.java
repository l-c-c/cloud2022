package com.wenxi.nacos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author shiwenxi
 * @date 2022-07-15 9:04
 */
@SpringBootApplication
@EnableDiscoveryClient
public class Payment9001 {
	public static void main(String[] args) {
		SpringApplication.run(Payment9001.class,args);
	}
}
