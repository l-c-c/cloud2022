package com.wenxi.nacos.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shiwenxi
 * @date 2022-07-15 11:01
 */
@RestController
@Slf4j
public class PaymentController {

	@Value("${server.port}")
	private String serverPort;

	@GetMapping("/payment/nacos/{id}")
	public String getPayment(@PathVariable("id") Integer id){
		return "Nacos registry, serverPort:" + serverPort + "id"+ id;
	}
}
