package com.ultrapower.springcloud.lb;

import org.springframework.cloud.client.ServiceInstance;

import java.util.List;

/**
 * @author shiwenxi
 * @date 2022-05-24 19:13
 *
 * 自定义的轮询算法
 */
 public interface LoadBalancer {

	/**
	 * 获取服务实例的接口
	 * @param serviceInstances 传入的所有的服务实例的集合
	 * @return 指定的服务实例
	 */
	ServiceInstance instances(List<ServiceInstance> serviceInstances);
}
