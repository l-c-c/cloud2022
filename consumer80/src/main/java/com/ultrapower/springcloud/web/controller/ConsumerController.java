package com.ultrapower.springcloud.web.controller;

import com.ultrapower.springcloud.domain.CommonResult;
import com.ultrapower.springcloud.domain.Payment;
import com.ultrapower.springcloud.lb.LoadBalancer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.net.URI;
import java.util.List;

/**
 *
 * 由于客户端只有访问对应的支付微服务才能进行业务的进行
 * 所以消费者只有对应的控制器,去调用对应的支付微服务
 *
 * private  final Logger logger = LoggerFactory.getLogger(XXX.class);
 * 不想写对应的测试的日志对象,所以使用对应的日志的注解,可以替代对应语句
 * @author shiwenxi
 * @date 2021-12-25 23:09
 */
@RestController
@Slf4j
public class ConsumerController {


	@Resource
	private DiscoveryClient discoveryClient;
	/*RestTemplate 技术是对应的spring用于简化发起HTTP请求的以及响应过程的,并且支持对应的RestFul风格
	 *
	 */
	/**
	 * 自定义的负载均衡轮序算法接口
	 */
	@Resource
	private LoadBalancer loadBalancer;

	@Resource
	private RestTemplate restTemplate;


	public static final String PAYMENT_URL = "http://PAYMENT-SERVICE";
	/**
	 * 对应的postForObject的参数解析
	 * URL  :对应的服务端的http地址
	 * 传入的对应的参数
	 * 对应的返回结果的class文件,加载对应的CommonResult的类
	 * @return 返回前端的结果
	 *
	 * 添加方法
	 */
	@GetMapping (value = "/consumer/payment/create")
	public CommonResult create(Payment payment){
		return restTemplate.postForObject(PAYMENT_URL+"/payment/create",payment,CommonResult.class);
	}



	@GetMapping(value = "/consumer/payment/get/{id}")
	public CommonResult getById(@PathVariable("id") String id){
		return restTemplate.getForObject(PAYMENT_URL+"/payment/get/"+id,CommonResult.class);
	}


	@GetMapping(value = "/consumer/payment/getForEntity/{id}")
	public CommonResult<Payment> getPayment(@PathVariable String id){
		ResponseEntity<CommonResult> entity = restTemplate.getForEntity(PAYMENT_URL + "/payment/get/" + id, CommonResult.class);
		if (entity.getStatusCode().is2xxSuccessful()) {
			return entity.getBody();
		}else {
			return new CommonResult(444,"操作失误!");
		}
	}

	@GetMapping(value = "/consumer/payment/discovery")
	public Object discovery(){
		//获取所有服务
		List<String> services = discoveryClient.getServices();
		for (String service : services) {
			log.info("----------------service:" + service);
		}
		//获取所有的服务实例
		List<ServiceInstance> instances = discoveryClient.getInstances("PAYMENT-SERVICE");
		for (ServiceInstance instance : instances) {
			log.info("=====================instance" + instance);
			System.out.println(instance.getUri().toString() + "*******************");
		}
		return this.discoveryClient;
	}


	@GetMapping(value = "/consumer/payment/lb")
	public String getPaymentLb(){
		//获取eureka中注册的所有的服务实例
		List<ServiceInstance> instances = discoveryClient.getInstances("PAYMENT-SERVICE");
		//得到当前的使用的服务实例
		ServiceInstance serviceInstance = loadBalancer.instances(instances);
		//获取该实例的访问地址前缀URL
		URI uri = serviceInstance.getUri();
		return restTemplate.getForObject(uri + "/payment/lb",String.class);

	}
}
