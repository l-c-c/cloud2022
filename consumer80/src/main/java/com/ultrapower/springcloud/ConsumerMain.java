package com.ultrapower.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * 配置对应的启动类
 * @author shiwenxi
 * @date 2021-12-25 23:01
 */
@SpringBootApplication
@EnableEurekaClient
//加载自定义的负载均衡算法
//@RibbonClient(name = "PAYMENT-SERVICE",configuration = MySelfRule.class)
public class ConsumerMain {
	public static void main(String[] args) {
		SpringApplication.run(ConsumerMain.class,args);
	}
}
