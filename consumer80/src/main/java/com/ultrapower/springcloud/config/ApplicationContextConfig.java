package com.ultrapower.springcloud.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

/**
 *
 * 配置类,用于配置对应的spring的bean管理类的类
 * @author shiwenxi
 * @date 2021-12-26 11:05
 */
@Configuration
public class ApplicationContextConfig {


	@Bean
	public ClientHttpRequestFactory simpleClientHttpRequestFactory(){
		SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
//		设置对应的读取时间
		factory.setReadTimeout(5000);
//		设置连接超时时间
		factory.setConnectTimeout(15000);
		return factory;
	}

	/*
	  spring管理的对应的RestTemplate的对象的注入,@LoadBalanced是为restTemplate添加负载均衡
	  便于动态获取对应的支付端的服务器,自动切换需要访问的对应的服务. 使用ribbon
	 */

	@Bean
	@LoadBalanced
	public RestTemplate getRestTemplate(ClientHttpRequestFactory factory){
		return new RestTemplate(factory);
	}

}
