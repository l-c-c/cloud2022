package com.ultrapower.myrule;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 自定义Ribbon负载均衡规则类
 * @author shiwenxi
 * @date 2022-05-20 10:33
 */
@Configuration
public class MySelfRule{


	/**
	 * 自定义返回IRule对象
	 * @return IRule
	 */
	@Bean
	public IRule myRule(){
		//需要返回随机的Rule对象
		return new RandomRule();
	}
}
