package com.cloud.wenxi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @auther shiwenxi
 * @date 2022-10-20
 */
@SpringBootApplication
public class NacosProviderApplicationCopy {
    public static void main(String[] args) {
        SpringApplication.run(NacosProviderApplicationCopy.class,args);
    }
}
