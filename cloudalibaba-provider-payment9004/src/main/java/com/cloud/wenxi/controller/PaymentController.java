package com.cloud.wenxi.controller;

import com.ultrapower.springcloud.domain.CommonResult;
import com.ultrapower.springcloud.domain.Payment;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

/**
 * @auther shiwenxi
 * @date 2022-10-20
 */
@RestController
public class PaymentController {

    @Value("${server.port}")
    private String serverPort;

    public static HashMap<Long, Payment> hashMap = new HashMap<>();

    static {
        hashMap.put(1L, new Payment(1, "2dfdsadsdfa2181"));
        hashMap.put(2L, new Payment(2, "3fadfasdasd2182"));
        hashMap.put(3L, new Payment(3, "4dasdassdas2183"));
    }

    @GetMapping(value = "/paymentSQL/{id}")
    public CommonResult<?> paymentSQL(@PathVariable("id") Long id) {
        Payment payment = hashMap.get(id);
        return new CommonResult<>(200, "from mysql,serverPort: " + serverPort, payment);
    }
}
