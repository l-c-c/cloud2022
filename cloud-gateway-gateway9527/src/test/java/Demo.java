import java.time.ZonedDateTime;

/**
 * @author shiwenxi
 * @date 2022-07-07 8:36
 */
public class Demo {
	public static void main(String[] args) {
		//默认时区
		ZonedDateTime zonedDateTime = ZonedDateTime.now();
		System.out.println(zonedDateTime);
	}
}
