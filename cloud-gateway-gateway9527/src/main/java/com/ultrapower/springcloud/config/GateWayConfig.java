package com.ultrapower.springcloud.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author shiwenxi
 * @date 2022-07-05 0:15
 */
@Configuration
public class GateWayConfig {
	/**
	 * 将对应的/game映射到对应的uri地址上面
	 * @param routeLocatorBuilder 创建路由的builder对象
	 * @return
	 */
	@Bean
	public RouteLocator customRouteLocator(RouteLocatorBuilder routeLocatorBuilder){
		RouteLocatorBuilder.Builder routes = routeLocatorBuilder.routes();
		//第一个参数是对应的路由的id名称(保证唯一性就行)   x->x.path() 映射的地址名称     uri,被映射的地址
		return routes.route("path_route_atwenxi",predicateSpec -> predicateSpec.path("/news").uri("http://erp.hfvast.com/login.do")
		).build();
	}
}
