package com.wenxi.cloud;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author shiwenxi
 * @date 2022-05-18 21:56
 */
//启动的时候剔除对应的自动配置的配置类
@SpringBootApplication(exclude = {DruidDataSourceAutoConfigure.class, DataSourceAutoConfiguration.class})
@EnableDiscoveryClient
public class ConsumerMain {
	public static void main(String[] args) {
		SpringApplication.run(ConsumerMain.class,args);
	}
}
