package com.wenxi.cloud.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

/**
 * @author shiwenxi
 * @date 2022-05-18 21:59
 */
@Configuration
public class SpringConfig {

	@Bean
	public ClientHttpRequestFactory simpleClientHttpRequestFactory(){
		SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
//		设置对应的读取时间
		factory.setReadTimeout(5000);
//		设置连接超时时间
		factory.setConnectTimeout(15000);
		return factory;
	}



	@Bean
	@LoadBalanced
	public RestTemplate getRestTemplate(ClientHttpRequestFactory factory){
		return new RestTemplate(factory);
	}
}
