package com.wenxi.cloud.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * @author shiwenxi
 * @date 2022-05-18 22:06
 */
@RestController
@Slf4j
public class OrderZkController {

	/**
	 * 服务提供者注册在注册中心中的服务名称地址
	 */
	private static final String INVOKE_URL = "http://cloud-provider-payment";

	@Resource
	private RestTemplate restTemplate;


	@RequestMapping(value = "/consumer/payment/zk")
	public String paymentZk(){
		return restTemplate.getForObject(INVOKE_URL + "/payment/zk",String.class);
	}
}
