package com.wenxi.cloud.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author shiwenxi
 * @date 2022-07-18 12:29
 */
@Configuration
public class ApplicationContextConfig {

	/**
	 * 负载均衡一般都要生成   千万不要忘了负载均衡注解
	 * @return
	 */
	@Bean
	@LoadBalanced
	public RestTemplate getRestTemplate(){
		return new RestTemplate();
	}
}
