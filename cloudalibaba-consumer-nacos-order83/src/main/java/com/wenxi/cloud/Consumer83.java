package com.wenxi.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author shiwenxi
 * @date 2022-07-18 12:08
 */
@SpringBootApplication
@EnableDiscoveryClient
public class Consumer83 {
	public static void main(String[] args) {
		SpringApplication.run(Consumer83.class,args);
	}
}
